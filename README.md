# Which terminal do you use
Add a little function to your ~/.bashrc that returns the name and version of the terminal emulator you're using (this works for most common terminal emulators)

- run on term => $ which_term 
- result => gnome-terminal  3.10.1-1
